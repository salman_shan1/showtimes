require 'nokogiri'
require 'open-uri'
class Movies
	class << self
		def fetch(city,date)
			doc = Nokogiri::HTML(open("http://www.showtimes.pk/requests/get-movies.php?cityId="+city.to_s+"&date="+date.to_s+""))
			movies =  doc.css("ul li")
			movies.each_with_index do |m,index|
				movie = Movie.new
				movie.name = m.xpath('//a/div')[index].text
				movie.link = m.xpath('//a/@href')[index].value 
				movie.city_web_id = city
				movie.movie_date = date
				movie.movie_web_id = m.xpath('//a/div')[index].attributes["id"].value.gsub("m-","")
				movie.save
			end
			return true
		end
		def fetch_detail(city_web_id,movie_web_id)
			movie = Movie.where(:movie_web_id => movie_web_id).last
			city = City.where(:city_web_id => city_web_id).last
			doc = Nokogiri::HTML(open(movie.link))
			cinemas = doc.css("#cboCinemas option")
			cinemas.each do |cinema|
				c = Cinema.new
				c.name = cinema.text
				c.city_id = city.id
				c.cinema_web_id = cinema.attributes["value"].value 
				c.save
			end
			city.cinemas.each do |cinema|
				check = cinema.movies.where(:id => movie.id)
				if check.blank?
					doc = Nokogiri::HTML(open("http://www.showtimes.pk/requests/get-showtimes.php?cinemaId="+cinema.cinema_web_id.to_s+"&movieId="+movie_web_id.to_s+""))
					times = doc.css("b")
					times.each do |t|
						mtime = Movietime.new
						mtime.movie_date = t.children.text
						mtime.movie_id = movie.id
						mtime.save
					end
				end
			end
		end
	end
end