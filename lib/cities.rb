require 'nokogiri'
require 'open-uri'
class Cities
	class << self
		def fetch
			doc = Nokogiri::HTML(open("http://www.showtimes.pk/"))
			cities =  doc.css("#menuHolder ul li")
			cities.each do |c|
				city = City.new
				city.name = c.text
				city.city_web_id = c.attributes.first[1].value.scan( /\d/ ).first
				city.save
			end
		end
	end
end