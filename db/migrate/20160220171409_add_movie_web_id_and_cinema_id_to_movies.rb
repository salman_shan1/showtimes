class AddMovieWebIdAndCinemaIdToMovies < ActiveRecord::Migration
  def change
    add_column :movies, :movie_web_id, :integer
    add_column :movies, :cinema_id, :integer
    add_column :movies, :movie_times, :string
  end
end
