class CreateCinemas < ActiveRecord::Migration
  def change
    create_table :cinemas do |t|
      t.string :name
      t.date :movie_date
      t.string :movie_times
      t.belongs_to :city
      t.timestamps null: false
    end
  end
end
