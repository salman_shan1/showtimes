class CreateCinemasMovies < ActiveRecord::Migration
  def change
    create_table :cinemas_movies do |t|
      t.integer :cinema_id
      t.integer :movie_id

      t.timestamps null: false
    end
  end
end
