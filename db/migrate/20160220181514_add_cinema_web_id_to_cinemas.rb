class AddCinemaWebIdToCinemas < ActiveRecord::Migration
  def change
    add_column :cinemas, :cinema_web_id, :integer
  end
end
