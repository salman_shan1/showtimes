class CreateMovietimes < ActiveRecord::Migration
  def change
    create_table :movietimes do |t|
      t.string :movie_time
      t.date :movie_date
      t.belongs_to :movie
      t.timestamps null: false
    end
  end
end
