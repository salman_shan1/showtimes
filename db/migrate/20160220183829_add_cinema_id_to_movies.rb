class AddCinemaIdToMovies < ActiveRecord::Migration
  def change
    add_column :movies, :cinema_id, :integer
  end
end
