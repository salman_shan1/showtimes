class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :name
      t.date :movie_date
      t.string :link
      t.belongs_to :city
      t.timestamps null: false
    end
  end
end
