require 'cities'
require 'movies'
class HomeController < ApplicationController
  def index
  	@cities = City.all
  end
  def fetch_movies
  	@movies = Movie.where(:city_web_id => params[:city][:name],:movie_date => params[:date])
  	if @movies.blank?
  		fetch_movies = Movies.fetch(params[:city][:name],params[:date])	
  		if fetch_movies
  			@movies = Movie.where(:city_web_id => params[:city][:name],:movie_date => params[:date])
  		end
  	end
  	@city = City.where(:city_web_id => params[:city][:name]).last
  	@date = params[:date]
  end
  def get_detail
  	@movie = Movie.where(:movie_web_id => params[:id]).last
  	city = City.where(:city_web_id => params[:city_id]).last
  	@cinemas = city.cinemas
  	if @movie.movietimes.blank?
  		fetch_movie_detail = Movies.fetch_detail(params[:city_id],params[:id])	
  	end
  end
  def update_cities
  	cities = City.all
  	if cities.present?
	  	cities.destroy_all
	  end	
  	Cities.fetch
  	redirect_to request.referer, :notice => "Successfully Updated!"
  end
end
	